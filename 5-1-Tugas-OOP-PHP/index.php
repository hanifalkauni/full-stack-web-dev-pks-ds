<?php

abstract class Hewan{
    public $nama;
    public $darah;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
        echo $this->nama." sedang ".$this->keahlian;
    }
}

abstract class Fight Extends hewan {
    public $attackPower;
    public $defensePower;

    public function serang($lawan){
        $lawan->darah = $lawan->darah - $this->attackPower / $lawan->defensePower;
        echo $this->nama . " sedang menyerang ".$lawan->nama;
        echo "<br>";
        $lawan->diserang();

    }
    public function diserang(){
        echo $this->nama . " sedang diserang<br>";
    }
}

class Harimau extends Fight {
    public function __construct(){
        $this->nama = "Harimau";
        $this->darah = 50;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defensePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis : ".$this->nama."<br>";
        echo "Jumlah Kaki : ".$this->jumlahKaki."<br>";
        echo "Keahlian : ".$this->keahlian."<br>";
        echo "Darah : ".$this->darah."<br>";
        echo "Attack Power : ".$this->attackPower."<br>";
        echo "Defense Power : ".$this->defensePower."<br>";
    }
}

class Elang extends Fight {
    public function __construct(){
        $this->nama = "elang";
        $this->darah = 50;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defensePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis : ".$this->nama."<br>";
        echo "Jumlah Kaki : ".$this->jumlahKaki."<br>";
        echo "Keahlian : ".$this->keahlian."<br>";
        echo "Darah : ".$this->darah."<br>";
        echo "Attack Power : ".$this->attackPower."<br>";
        echo "Defense Power : ".$this->defensePower."<br>";
    }
}

$harimau = new Harimau;
$elang = new Elang;

echo "<h3>Info awal</h3>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();


echo "<br><h3>Serangan pertama</h3>";
$elang->serang($harimau);
echo "<h4>info pasca serangan</h4>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();

echo "<br><h3>Serangan kedua</h3>";
$harimau->serang($elang);
echo "<h4>info pasca serangan</h4>";
$elang->getInfoHewan();
echo "<br>";
$harimau->getInfoHewan();

?>
