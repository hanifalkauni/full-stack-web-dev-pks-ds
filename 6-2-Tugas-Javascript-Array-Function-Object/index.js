// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var daftarHewanUrut = daftarHewan.sort()
for (let index = 0; index < daftarHewanUrut.length; index++) {
  console.log(daftarHewanUrut[index]);
    
}

// soal 2 

function introduce(data){
    return "Nama saya "+data.name+", umur saya "+data.age+" tahun, alamat di "+data.address+", dan saya punya hobby yaitu "+data.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


//soal 3
function hitung_huruf_vokal(name){
    let vokal = ['a','i','u','e','o']
    let count=0
    name = name.toLowerCase()
    for (let index = 0; index < name.length; index++) {
        for (let index2 = 0; index2 < vokal.length; index2++) {
            name[index]==vokal[index2] ? count++ : count ;
        }
    }
    return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


//soal 4
function hitung(int){
    let result = -2
    for (let index = 0; index < int; index++) {
        result+=2 
    }
    return result
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8
